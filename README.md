# unofficial-patches

Unofficially patched SlackBuilds for Slackware and SlackBuilds.org

## make (Slackware)

Patched to prevent Gnu make from locking up with zombie children. This
builds on both Slackware-14.2 and Slackware-current.

See these links:

* https://savannah.gnu.org/bugs/?49014
* https://savannah.gnu.org/bugs/?51159
* https://savannah.gnu.org/bugs/?51400
* https://git.savannah.gnu.org/cgit/make.git

## fakeroot (SBo)

Patched for Slackware-current, to prevent fakeroot from locking up.

See this link:

* https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=883780

## slacknetsetup (rlw)

Builds a package for Robby's new network setup scripts, built from his
git repo.

* https://git.rlworkman.net/slacknetsetup/
